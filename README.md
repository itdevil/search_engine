# search_engine


# How to install
1 Clone the project
2 Import to intellij pro
3 Install plugin lombok
4 Run SearchEngineApplication to start the project
It take about 5 minutes for first start ( create indexing database )
5 Go to http://localhost:8080/swagger-ui.html for playing with apis
6 Run test case : SearchServiceImplTest for generating report 2 ways for searching

