package com.lucky.searchengine.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lucky.searchengine.SearchEngineApplication;
import com.lucky.searchengine.domain.ProductEntity;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchEngineApplication.class)

public class ProductServiceTest {

    @Autowired
    public ProductService productService;

    @Test
    public void testInsertProduct() {
        String productName = "Loc test insert Apple iMac 1";
        ProductEntity result = productService.insertProduct(productName);

        System.out.println(result.toString());
    }
}
