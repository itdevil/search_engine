package com.lucky.searchengine.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lucky.searchengine.SearchEngineApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchEngineApplication.class)
public class InitializeDataServiceImplTest {

    @Autowired
    public InitializeDataService initializeDataService;

    @Test
    public void testInitializeDataTest() {

        String pathFile = "/Users/locdt/Desktop/product_names.txt";
        initializeDataService.insertProductNames(pathFile);
        initializeDataService.createInvertedIndex();
    }
}
