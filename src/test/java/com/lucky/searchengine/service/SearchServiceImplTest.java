package com.lucky.searchengine.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import com.lucky.searchengine.SearchEngineApplication;
import com.lucky.searchengine.domain.ProductEntity;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchEngineApplication.class)
public class SearchServiceImplTest {

    @Autowired
    public SearchService searchService;

    @Autowired
    private Environment env;

    @Test
    public void testSearchMethod() {
        String query = "sách";
        List<ProductEntity> docs = searchService.search(query);

        System.out.println(docs);
        System.out.println(docs.size());
    }

    @Test
    public void testSearchAndRanking() {
        String query = "tai nghe";
        List<ProductEntity> docs = searchService.searchAndRanking(query);

        System.out.println(docs);
        System.out.println(docs.size());
    }

    @Test
    public void testCompareSearchAndSearchWithRanking() {
        String query_path = env.getProperty("query_test");
        try {
            List<String> lines = Files.readAllLines(Paths.get(query_path));

            String[] columns = {
                    "keyword", "keyword search", "full text search",
                    "Score"
            };

            Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Contacts");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.RED.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create a Row
            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowNum = 1;

            for (String line : lines) {
                System.out.println(line);
                List<ProductEntity> keywordSearch = searchService.search(line);
                if (keywordSearch.size() > 10) {
                    keywordSearch = keywordSearch.subList(0, 9);
                }
                List<ProductEntity> fullTextSearch = searchService.searchAndRanking(line);
                if (fullTextSearch.size() > 10) {
                    fullTextSearch = fullTextSearch.subList(0, 9);
                }
                Iterator<ProductEntity> k = keywordSearch.iterator();
                Iterator<ProductEntity> f = fullTextSearch.iterator();

                while (k.hasNext() | f.hasNext()) {
                    Row row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue(line);
                    row.createCell(1).setCellValue(k.hasNext() ? k.next().getProductName() : "");
                    if (f.hasNext()) {
                        ProductEntity fProduct = f.next();
                        row.createCell(2).setCellValue(fProduct.getProductName());
                        row.createCell(3).setCellValue(fProduct.getBestMatchScore());
                    } else {
                        row.createCell(2).setCellValue("");
                        row.createCell(3).setCellValue("");

                    }

                }

            }

            // Resize all columns to fit the content size
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

            // Write the output to a file
            FileOutputStream fileOut = new FileOutputStream("report.xlsx");
            workbook.write(fileOut);
            fileOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
