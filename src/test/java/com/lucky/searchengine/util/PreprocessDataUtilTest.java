package com.lucky.searchengine.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PreprocessDataUtilTest {

    @Test
    public void testremoveVietNameseAccent() {
        //GIVEN
        String input = "đầm a kiểu croptop phủ voan hàn quốc, đính nơ cách điệu tosonfashion 47006h328 (size s)";

        // WHEN
        String removedAccent = PreprocessDataUtil.removeVietNameseAccent(input);

        // THEN
        String expectedOutPut = "dam a kieu croptop phu voan han quoc, dinh no cach dieu tosonfashion 47006h328 (size s)";

        Assert.assertEquals(expectedOutPut,removedAccent);
    }
}
