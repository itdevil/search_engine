package com.lucky.searchengine.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class NgramUtilTest {

    @Test
    public void testGenerateTokens() throws Exception {
        //GIVEN
        String input =
                "đầm a kiểu croptop phủ voan hàn quốc, đính nơ phủ cách điệu tosonfashion 47006h328 (size s)";

        // WHEN
        List<String> removedAccent = NgramUtil.generateTokens( 6, input);

        // THEN
        System.out.println(removedAccent.size());

    }
}
