package com.lucky.searchengine.service;

import com.lucky.searchengine.domain.ProductEntity;

public interface ProductService {

    ProductEntity insertProduct(String productname);

    void insertInvertedDocument(ProductEntity product);
}
