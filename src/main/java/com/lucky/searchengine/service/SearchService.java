package com.lucky.searchengine.service;

import java.util.List;

import com.lucky.searchengine.domain.ProductEntity;

public interface SearchService {

    List<ProductEntity> search(String query);

    List<ProductEntity> searchAndRanking(String query);

}
