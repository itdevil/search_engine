package com.lucky.searchengine.service.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.lucky.searchengine.domain.ProductEntity;
import com.lucky.searchengine.repository.InvertedIndexDocumentRepository;
import com.lucky.searchengine.repository.ProductRepository;
import com.lucky.searchengine.service.InitializeDataService;
import com.lucky.searchengine.service.ProductService;
import com.lucky.searchengine.util.PreprocessDataUtil;

@Service
public class InitializeDataServiceImpl implements InitializeDataService {

    @Autowired
    public ProductRepository productRepository;

    @Autowired
    public InvertedIndexDocumentRepository invertedIndexDocumentRepository;

    @Autowired
    public ProductService productService;

    public final int MAX_BUNDLE = 100;

    @Override
    public void insertProductNames(String pathFile) {
        FileInputStream inputStream = null;
        Scanner sc = null;
        try {
            inputStream = new FileInputStream(pathFile);
            sc = new Scanner(inputStream, "UTF-8");

            List<ProductEntity> batch = new ArrayList<>(MAX_BUNDLE);

            while (sc.hasNextLine()) {
                String line = sc.nextLine().trim().toLowerCase();

                line = PreprocessDataUtil.removeSpecialChar(line);
                line = StringUtils.normalizeSpace(line);

                String preprocessed = PreprocessDataUtil.removeVietNameseAccent(line);
                batch.add(new ProductEntity(line, preprocessed));

                if (batch.size() == MAX_BUNDLE) {
                    // begin process
                    productRepository.saveAll(batch);
                    batch.clear();
                }
            }

            if (!batch.isEmpty()) {
                productRepository.saveAll(batch);
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (sc != null) {
                sc.close();
            }
        }
    }

    @Override
    public boolean isEmptyDatabase() {
        return productRepository.count() < 1;
    }

    @Override
    public void createInvertedIndex() {

        long numberOfProduct = productRepository.count();
        long countPage = numberOfProduct / 100;
        if (numberOfProduct % 100 != 0) {
            countPage = countPage + 1;
        }

        for (int i = 0; i < countPage; i++) {
            PageRequest request = PageRequest.of(i, 100);

            List<ProductEntity> products = productRepository.findAll(request).getContent();
            for (ProductEntity product : products) {
                productService.insertInvertedDocument(product);

            }

        }

    }
}
