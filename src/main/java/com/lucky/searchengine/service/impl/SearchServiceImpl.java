package com.lucky.searchengine.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lucky.searchengine.algorithm.BestMatchRanking;
import com.lucky.searchengine.domain.InvertedIndexDocumentEntity;
import com.lucky.searchengine.domain.ProductEntity;
import com.lucky.searchengine.repository.InvertedIndexDocumentRepository;
import com.lucky.searchengine.service.SearchService;
import com.lucky.searchengine.util.PreprocessDataUtil;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    InvertedIndexDocumentRepository invertedIndexDocumentRepository;

    @Override
    public List<ProductEntity> search(String query) {
        boolean accent = !PreprocessDataUtil.isAsciiPrintable(query);
        query = query.toLowerCase();
        List<InvertedIndexDocumentEntity> result =
                invertedIndexDocumentRepository.findByKeywordAndAccentOrderByOccurrencesDesc(
                        query, accent);

        List<ProductEntity> products = result.stream().map(InvertedIndexDocumentEntity::getProduct).distinct().collect(
                Collectors.toList());
        return products;
    }

    @Override
    public List<ProductEntity> searchAndRanking(String query) {

        boolean accent = !PreprocessDataUtil.isAsciiPrintable(query);
        String preprocessedQuery = PreprocessDataUtil.removeSpecialChar(query);
        preprocessedQuery = StringUtils.normalizeSpace(preprocessedQuery);
        String[] terms = preprocessedQuery.split(" ");

        Set<ProductEntity> products = new HashSet<>();
        for (String term : terms) {
            List<ProductEntity> productContainsTerm = search(term);
            products.addAll(productContainsTerm);
        }

        if (products.isEmpty()) {
            return Collections.emptyList();
        }

        BestMatchRanking ranking = new BestMatchRanking(terms, new ArrayList<>(products), accent);

        return ranking.rankingDocuments();
    }

}
