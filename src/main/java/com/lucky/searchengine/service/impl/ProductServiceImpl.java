package com.lucky.searchengine.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lucky.searchengine.domain.InvertedIndexDocumentEntity;
import com.lucky.searchengine.domain.ProductEntity;
import com.lucky.searchengine.repository.InvertedIndexDocumentRepository;
import com.lucky.searchengine.repository.ProductRepository;
import com.lucky.searchengine.service.ProductService;
import com.lucky.searchengine.util.NgramUtil;
import com.lucky.searchengine.util.PreprocessDataUtil;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    public ProductRepository productRepository;

    @Autowired
    public InvertedIndexDocumentRepository invertedIndexDocumentRepository;

    @Override
    @Transactional
    public ProductEntity insertProduct(String productName) {
        productName = PreprocessDataUtil.removeSpecialChar(productName);
        productName = StringUtils.normalizeSpace(productName);

        String preprocessed = PreprocessDataUtil.removeVietNameseAccent(productName);
        ProductEntity product = new ProductEntity(productName, preprocessed);
        product = productRepository.save(product);

        insertInvertedDocument(product);

        return product;

    }

    @Override
    public void insertInvertedDocument(ProductEntity product) {
        try {
            String productName = product.getProductName();
            List<String> tokens = NgramUtil.generateTokens(6, productName);
            List<InvertedIndexDocumentEntity> documents = new ArrayList<>(tokens.size());
            for (String token : tokens) {
                if (PreprocessDataUtil.isAsciiPrintable(token)) {
                    InvertedIndexDocumentEntity document = new InvertedIndexDocumentEntity(
                            token, PreprocessDataUtil.countWords(token, " "), product,
                            StringUtils.countMatches(productName, token),
                            false
                    );
                    documents.add(document);
                } else {
                    String notAccent = PreprocessDataUtil.removeVietNameseAccent(token);
                    InvertedIndexDocumentEntity notAccentDocument = new InvertedIndexDocumentEntity(
                            notAccent, PreprocessDataUtil.countWords(token, " "), product,
                            StringUtils.countMatches(product.getProductNameNotAccent(), notAccent),
                            false
                    );

                    InvertedIndexDocumentEntity document = new InvertedIndexDocumentEntity(
                            token, PreprocessDataUtil.countWords(token, " "), product,
                            StringUtils.countMatches(productName, token),
                            true
                    );

                    documents.add(document);
                    documents.add(notAccentDocument);

                }
            }

            invertedIndexDocumentRepository.saveAll(documents);

        } catch (Exception e) {

        }
    }
}
