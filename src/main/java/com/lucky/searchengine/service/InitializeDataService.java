package com.lucky.searchengine.service;

public interface InitializeDataService {

    void insertProductNames(String pathFile);

    boolean isEmptyDatabase();

    void createInvertedIndex();
}
