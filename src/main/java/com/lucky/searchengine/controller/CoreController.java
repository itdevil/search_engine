package com.lucky.searchengine.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lucky.searchengine.controller.domain.Product;
import com.lucky.searchengine.domain.ProductEntity;
import com.lucky.searchengine.service.ProductService;
import com.lucky.searchengine.service.SearchService;

@RestController
@RequestMapping("/")
public class CoreController {

    @Autowired
    public SearchService searchService;

    @Autowired
    public ProductService productService;

    @GetMapping(path = "/search/{query}")
    public List<Product> searchProduct(@PathVariable("query") final String query) {

        List<ProductEntity> products = searchService.search(query);

        return products.stream().map(product -> new Product(product.getProductName(), product.getId()))
                       .collect(
                               Collectors.toList());

    }

    @PostMapping
    public ResponseEntity<Product> insertProduct(@RequestBody Product product) {
        ProductEntity productEntity = productService.insertProduct(product.getProductName());
        return ResponseEntity.ok(new Product(productEntity.getProductName(), productEntity.getId()));
    }
}
