package com.lucky.searchengine.controller.domain;

import lombok.Data;

@Data
public class Product {

    private String productName;
    private Long productID;

    public Product() {
    }

    public Product(String productName, Long productID) {
        this.productName = productName;
        this.productID = productID;
    }
}
