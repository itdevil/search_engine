package com.lucky.searchengine.util;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class NgramUtil {

    public static List<String> generateTokens(int max, String input) throws IOException {
        int length = input.split(" ").length;
        Reader reader = new StringReader(input);
        TokenStream tokenizer = new ShingleFilter(new StandardTokenizer(Version.LUCENE_35, reader), 2, max);
        CharTermAttribute charTermAttribute = tokenizer.addAttribute(CharTermAttribute.class);
        List<String> tokens = new ArrayList<>(length * max);
        while (tokenizer.incrementToken()) {
            String token = charTermAttribute.toString();
            tokens.add(token);
        }

        return tokens;
    }


}
