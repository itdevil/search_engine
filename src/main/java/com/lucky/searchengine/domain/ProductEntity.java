package com.lucky.searchengine.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
public class ProductEntity {

    public ProductEntity() {

    }

    @Id
    @GeneratedValue
    @Column(name = "PRODUCT_ID")
    private Long id;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "PRODUCT_NAME_NOT_ACCENT")
    private String productNameNotAccent;

    public ProductEntity(String productName, String productNameNotAccent) {
        this.productName = productName;
        this.productNameNotAccent = productNameNotAccent;
    }

    @Transient
    private Double bestMatchScore;
}
