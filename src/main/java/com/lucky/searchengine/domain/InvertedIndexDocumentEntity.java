package com.lucky.searchengine.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(indexes = {
        @Index(name = "keyword_index", columnList = "KEYWORD,ACCENT")
}
)

public class InvertedIndexDocumentEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "KEYWORD")
    private String keyword;

    @Column(name = "NGRAM")
    private Integer ngram;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;

    @Column(name = "OCCURRENCES")
    private Integer occurrences;

    @Column(name = "ACCENT")
    private Boolean accent;

    public InvertedIndexDocumentEntity(String keyword, Integer ngram, ProductEntity product, Integer occurrences,
                                       Boolean accent) {
        this.keyword = keyword;
        this.ngram = ngram;
        this.product = product;
        this.occurrences = occurrences;
        this.accent = accent;
    }

    public InvertedIndexDocumentEntity() {
    }
}
