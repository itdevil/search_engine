package com.lucky.searchengine.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.lucky.searchengine.service.InitializeDataService;

@Component
public class StartUpInit {
    @Autowired
    InitializeDataService initializeDataService;

    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        // initial data if it is empty
        if (initializeDataService.isEmptyDatabase()) {
            initializeDataService.insertProductNames(env.getProperty("init_data"));
            initializeDataService.createInvertedIndex();

        }
    }
}
