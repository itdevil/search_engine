package com.lucky.searchengine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lucky.searchengine.domain.InvertedIndexDocumentEntity;

public interface InvertedIndexDocumentRepository extends JpaRepository<InvertedIndexDocumentEntity, Long> {

    List<InvertedIndexDocumentEntity> findByKeywordAndAccentOrderByOccurrencesDesc(String keyword, boolean accent);
}
