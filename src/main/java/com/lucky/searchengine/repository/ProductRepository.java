package com.lucky.searchengine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lucky.searchengine.domain.ProductEntity;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

}
