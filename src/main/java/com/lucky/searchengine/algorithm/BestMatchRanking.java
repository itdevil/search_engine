package com.lucky.searchengine.algorithm;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.lucky.searchengine.domain.ProductEntity;
import com.lucky.searchengine.util.PreprocessDataUtil;

public class BestMatchRanking {

    // 1.2 or 2.0
    public static final Double K1 = 2.0;

    public static final Double B = 0.75;

    private Double avgdl;

    private String[] terms;
    private List<ProductEntity> documents;
    private boolean accent;

    public BestMatchRanking(String[] terms, List<ProductEntity> documents, boolean accent) {
        this.terms = terms;
        this.documents = documents;
        this.accent = accent;
        this.avgdl = this.calAverageLengthDocument();
    }

    public int calTermFrequency(String term, String doc) {
        return StringUtils.countMatches(doc, term);
    }

    public int calNumDocContainsTerm(String term) {

        return documents.stream()
                        .filter(doc -> accent ? doc.getProductName().contains(term) :
                                       doc.getProductNameNotAccent().contains(term)
                        )
                        .collect(Collectors.toList())
                        .size();
    }

    public Double calInverseDocumentFrequency(String term) {

        int numOfDoc = documents.size();
        int numDocContainsTerm = calNumDocContainsTerm(term);
        double idf = Math.log((numOfDoc - numDocContainsTerm + 0.5) / (numDocContainsTerm + 0.5));
        return idf > 0 ? idf : 0.1;
    }

    public Double calAverageLengthDocument() throws RuntimeException {

        if (documents.isEmpty()) {
            throw new RuntimeException("There are no document for scoring");
        } else {

            double sum = documents.stream()
                                  .map(doc -> PreprocessDataUtil
                                          .countWords(doc.getProductName(), " "))
                                  .mapToDouble(count -> count)
                                  .sum();

            return sum / documents.size();
        }
    }

    public Double calWeight(String doc) {
        int numberOfWord = PreprocessDataUtil.countWords(doc, " ");
        Double numwordPeravgdl = numberOfWord / this.avgdl;

        return K1 * (1 - B + (B * numwordPeravgdl));
    }

    public Double scoringTerm(String term, String doc) {
        double idf = calInverseDocumentFrequency(term);
        int tf = calTermFrequency(term, doc);
        double weight = calWeight(doc);

        return idf * ((tf * (K1 + 1)) / (tf + weight));
    }

    public List<ProductEntity> rankingDocuments() {

        for (ProductEntity product : documents) {
            double docScore = 0.0;
            for (String term : terms) {
                docScore = docScore + scoringTerm(term, accent ? product.getProductName() :
                                                        product.getProductNameNotAccent());
            }

            product.setBestMatchScore(docScore);
        }

        documents.sort((o1, o2) -> {
            if (o1.getBestMatchScore() > o2.getBestMatchScore()) {
                return -1;
            } else if (o1.getBestMatchScore() < o2.getBestMatchScore()) {
                return 1;
            } else {
                return 0;
            }
        });
        return documents;
    }

}
